import del from 'del';
import * as fs from 'fs-extra';
import nodeGlob from 'glob';
import {
  Options as SassOptions,
  render as renderSassCbf,
  Result as SassResult,
} from 'node-sass';
import * as path from 'path';
import postcss from 'postcss';
import * as util from 'util';

import postcssConfig from '../scss/postcss.config';

// Promisified functions.
const glob = util.promisify(nodeGlob);
const renderSass = util.promisify<SassOptions, SassResult>(renderSassCbf);

/**
 * Build CSS.
 */
async function build(): Promise<void> {
  await del('css');

  // All .scss files at the top level in src are entry points.
  const entrypoints = await glob('scss/src/*.scss');
  await Promise.all(entrypoints.map(compile));
}

/**
 * Build and output the given scss file.
 */
async function compile(infile: string): Promise<void> {
  const filename = path.basename(infile, '.scss');
  const outfile = `css/${filename}.css`;

  // TODO: Merge sass sourcemap with postcss sourcemap.

  const sassRenderResult = await renderSass({
    file: infile,
    outputStyle: 'expanded',
    // sourceMap: true,
    // sourceMapRoot: path.relative(path.dirname(outfile), '.'),
    outFile: outfile,
  });

  const postcssOptions = {
    ...postcssConfig.options,
    from: `css/${filename}.pre.css`,
    to: outfile,
  };

  const postcssResult = await (
    postcss(postcssConfig.plugins)
      .process(
        sassRenderResult.css.toString('utf-8'),
        postcssOptions,
      ) as PromiseLike<postcss.Result>
  );

  const filesBeingWritten: Array<Promise<void>> = [];

  // Write the css file.
  filesBeingWritten.push(fs.outputFile(outfile, postcssResult.css));

  // Write the map file (if there is one).
  if (postcssResult.map as unknown !== undefined) {
    filesBeingWritten.push(
      fs.outputFile(`${outfile}.map`, postcssResult.map.toString()),
    );
  }

  // Wait for all the files to be written.
  await Promise.all(filesBeingWritten);
}

// Start
(async (): Promise<void> => {
  await build();
})()
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
