import { exec as nodeExec } from 'child_process';
import * as fs from 'fs-extra';
import * as util from 'util';

// Promisified functions.
const exec = util.promisify(nodeExec);

// tslint:disable: max-line-length
const composerJson = {
  'name': 'drupal/ac_attr',
  'description': 'Using simple rules based on a form\'s structure, site administrators can set what the autocomplete attribute for each field should be.',
  'scripts': {
    'lint:docs': 'phpcs --standard=Drupal,DrupalPractice --extensions=txt,md --ignore=node_modules,vendor .',
    'lint:php': 'phpcs --standard=Drupal,DrupalPractice --extensions=php,module,inc,install,test,profile,theme,info --ignore=node_modules,vendor .',
  },
  'require-dev': {
    'dealerdirect/phpcodesniffer-composer-installer': '^0.5.0',
    'drupal/coder': '^8.3',
    'squizlabs/php_codesniffer': '^3.4',
  },
};
// tslint:enable: max-line-length

/**
 * Install things that need to be installed.
 */
async function install(): Promise<void> {
  if (!fs.existsSync('composer.json')) {
    await fs.writeJSON('composer.json', composerJson, { spaces: 2 });
  }

  console.log('$ composer install');
  const composerInstallResult = await exec('composer install');

  console.log(composerInstallResult.stdout);
  console.error(composerInstallResult.stderr);
}

// Start
(async (): Promise<void> => {
  await install();
})()
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
