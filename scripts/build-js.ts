import del from 'del';
import nodeGlob from 'glob';
import { rollup, RollupOptions } from 'rollup';
import * as util from 'util';

import { getConfig } from '../ts/rollup.config';

// Promisified functions.
const glob = util.promisify(nodeGlob);

/**
 * Build JS.
 */
async function build(): Promise<void> {
  await del('js');

  // All .ts (not .d.ts) files at the top level in src are entry points.
  const entrypoints = await glob('ts/src/*{[^d].ts,[^.]d.ts}');
  await Promise.all(
    entrypoints.map(async (input) => buildRollup(getConfig(input))),
  );
}

/**
 * Run rollup.
 */
async function buildRollup(
  rollupConfig: RollupOptions,
): Promise<void> {
  if (rollupConfig.output === undefined) {
    return Promise.reject(new Error('Rollup config output not defined.'));
  }
  const rollupBuild = await rollup(rollupConfig);
  await rollupBuild.write(rollupConfig.output);
}

// Start
(async (): Promise<void> => {
  await build();
})()
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
