<?php

/**
 * @file
 * Sample hooks demonstrating usage in Autocomplete Attribute.
 */

/**
 * @defgroup ac_attr_hooks
 * @{
 */

/**
 * Return types which cannot have an autocompete attribute added to them.
 *
 * These are the values that appear in #type in the Form API array.
 *
 * @param array $types
 *   An array of strings passed by reference.
 */
function hook_ac_attr_invalid_field_types_alter(array &$types) {
  $types[] = 'actions';
  $types[] = 'button';
  $types[] = 'checkboxes';
  $types[] = 'container';
  $types[] = 'form';
  $types[] = 'image_button';
  $types[] = 'markup';
  $types[] = 'radios';
  $types[] = 'submit';
  $types[] = 'value';
  $types[] = 'vertical_tabs';
  $types[] = 'weight';
}

/**
 * Return types which should be treated as a fieldset by this module.
 *
 * These are the values that appear in #type in the Form API array.
 *
 * @param array $types
 *   An array of strings passed by reference.
 */
function hook_ac_attr_fieldset_types_alter(array &$types) {
  $types[] = 'fieldset';
}

/**
 * Return form ids that should be ignored by this module.
 *
 * @param array $blacklist
 *   An array of strings passed by reference.
 */
function hook_ac_attr_form_id_blacklist_alter(array &$blacklist) {
  $blacklist[] = 'some_form_to_ignore';
}

/**
 * @}
 */
