import cssnano from 'cssnano';
import * as postcss from 'postcss';
import postcssFlexbugsFixes from 'postcss-flexbugs-fixes';
import postcssPresetEnv from 'postcss-preset-env';

const isProdcutionBuild = process.env.NODE_ENV === 'production';

const devPlugins: Array<postcss.AcceptedPlugin> = [
  postcssPresetEnv(),
  postcssFlexbugsFixes(),
];

const prodPlugins: Array<postcss.AcceptedPlugin> = [
  postcssPresetEnv(),
  postcssFlexbugsFixes(),
  cssnano({
    // Autoprefixer is part of the `preset-env`, no need to run it again.
    autoprefixer: false,
    discardComments: {
      removeAll: true,
    },
  }),
];

const options: postcss.ProcessOptions = {
  map: isProdcutionBuild ? undefined : { inline: false },
};

const config = {
  plugins: isProdcutionBuild ? prodPlugins : devPlugins,
  options,
};

// Configs are allow default exports.
// tslint:disable-next-line: no-default-export
export default config;
