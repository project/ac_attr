declare namespace Drupal {
  /**
   * Translate a string.
   */
  function t(str: string, args?: ReadonlyArray<string>, options?: object): string;

  /**
   * Toggle the collapsed state of a fieldset.
   */
  const toggleFieldset: ((fieldset: HTMLFieldSetElement) => void) | undefined;
}
