/**
 * The label displayed when the button will collapse all fieldsets.
 */
const collapseLabel = Drupal.t('Collapse All');

/**
 * The label displayed when the button will expand all fieldsets.
 */
const expandLabel = Drupal.t('Expand All');

/**
 * Setup the given button as a collapse/expand helper.
 */
export function setupCollapseExpandHelper(
  form: HTMLFormElement,
  button: HTMLElement,
): void {
  // Drupal.toggleFieldset not defined?
  if (Drupal.toggleFieldset === undefined) {
    // Don't show the button.
    button.setAttribute('style', 'display: none;');
    // Don't set up the button.
    return;
  }

  // Make the button of type button.
  button.setAttribute('type', 'button');

  const [
    collapsibleElements,
    collapsedElements,
  ] = getState(form);

  update(
    form,
    button,
    collapsibleElements.length === collapsedElements.length,
  );
}

/**
 * Collapse all the collapsibles in the given field.
 */
function collapseAll(
  form: HTMLFormElement,
  button: HTMLElement,
): (event: Event) => void {
  return (event) => {
    event.preventDefault();

    const [
      collapsibleElements,
      collapsedElements,
    ] = getState(form);

    let collapsedItems = 0;
    collapsibleElements.forEach((element) => {
      // tslint:disable-next-line: no-any
      if ((element as any).animating) {
        return;
      }
      if (!element.classList.contains('collapsed')) {
        // tslint:disable-next-line: no-any
        (element as any).animating = true;
        Drupal.toggleFieldset!(element);
        collapsedItems++;
      }
    });

    update(
      form,
      button,
      collapsibleElements.length === collapsedElements.length + collapsedItems,
    );
  };
}

/**
 * Expand all the collapsibles in the given field.
 */
function expandAll(
  form: HTMLFormElement,
  button: HTMLElement,
): (event: Event) => void {
  return (event) => {
    event.preventDefault();

    const [
      collapsibleElements,
      collapsedElements,
    ] = getState(form);

    let expandedItems = 0;
    collapsibleElements.forEach((element) => {
      // tslint:disable-next-line: no-any
      if ((element as any).animating) {
        return;
      }
      if (element.classList.contains('collapsed')) {
        // tslint:disable-next-line: no-any
        (element as any).animating = true;
        Drupal.toggleFieldset!(element);
        expandedItems++;
      }
    });

    update(
      form,
      button,
      collapsibleElements.length === collapsedElements.length + expandedItems,
    );
  };
}

/**
 * Get the state of the given form and the given button.
 */
function getState(
  form: HTMLFormElement,
): readonly [
  NodeListOf<HTMLFieldSetElement>,
  NodeListOf<HTMLFieldSetElement>,
] {
  return [
    form.querySelectorAll('.collapsible'),
    form.querySelectorAll('.collapsible.collapsed'),
  ];
}

/**
 * Update the button and the collapsible elements' state.
 */
function update(
  form: HTMLFormElement,
  button: HTMLElement,
  allCollapsed: boolean,
): void {
  let newLabel: string;

  if (allCollapsed) {
    button.addEventListener('click', expandAll(form, button), { once: true });
    newLabel = expandLabel;
  } else {
    button.addEventListener('click', collapseAll(form, button), { once: true });
    newLabel = collapseLabel;
  }

  if (button instanceof HTMLInputElement) {
    button.value = newLabel;
  } else {
    button.textContent = newLabel;
  }
}
