import { setupCollapseExpandHelper } from './imports/setupCollapseExpandHelper';

// Run the main function if everything is ready.
if (document.readyState === 'complete') {
  main();
}
// Otherwise wait for load event before running it.
else {
  window.addEventListener('load', main);
}

/**
 * The main function.
 */
function main(): void {
  const forms =
    document.querySelectorAll<HTMLFormElement>('form.ac-attr-admin');
  forms.forEach((form) => {
    const helperContainer = form.querySelector('.helpers');
    if (helperContainer === null) {
      return;
    }

    const collapseExpandButton =
      helperContainer.querySelector<HTMLElement>('.collapse-expand');

    if (collapseExpandButton !== null) {
      helperContainer.removeAttribute('style');
      setupCollapseExpandHelper(form, collapseExpandButton);
    }
  });
}
