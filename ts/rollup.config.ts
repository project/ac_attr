/**
 * Rollup Config.
 */

import { Plugin, RollupOptions } from 'rollup';
import rollupPluginBabel from 'rollup-plugin-babel';
import { terser as rollupPluginTerser } from 'rollup-plugin-terser';
import rollupPluginTypescript from 'rollup-plugin-typescript2';

const isProdcutionBuild = process.env.NODE_ENV === 'production';

const typescript = rollupPluginTypescript({
  tsconfig: 'ts/tsconfig.json',
  tsconfigOverride: {
    compilerOptions: {
      sourceMap: !isProdcutionBuild,
    },
  },
});

const babel = rollupPluginBabel({
  exclude: 'node_modules/**',
  extensions: ['.ts'],
  presets: [
    '@babel/preset-env',
  ],
});

const devPlugins: Array<Plugin> = [
  typescript,
  babel,
];

const prodPlugins: Array<Plugin> = [
  typescript,
  babel,
  rollupPluginTerser({
    ecma: 5,
    toplevel: true,
  }),
];

/**
 * Get the rollup config for the given input
 */
export function getConfig(input: string): RollupOptions {
  return {
    input,

    output: {
      dir: 'js',
      entryFileNames: '[name].js',
      format: 'iife',
      sourcemap: !isProdcutionBuild,
    },

    plugins: isProdcutionBuild ? prodPlugins : devPlugins,

    treeshake: {
      pureExternalModules: true,
      propertyReadSideEffects: false,
      annotations: true,
    },
  };
}
