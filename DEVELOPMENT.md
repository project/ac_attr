# Developmenting the Autocomplete Attribute (ac_attr) Module

## Getting Started

### Development Dependencies

Please ensure that both [Composer](https://getcomposer.org) and
[NodeJS](https://nodejs.org) version 8 or newer are installed on your
development machine (NodeJS comes bundled with [Yarn](https://yarnpkg.com)).

To install the development dependencies this project needs, at the project root
run:

```sh
yarn install
```

With the dependencies in place, there are now several scripts available.

### Compiling JavaScript and CSS

JavaScript and CSS for this module are both written in there own respective
superset langauges, TypeScript and SCSS respectively.

To compile these frontend languages, first ensure that you have installed the
[Development Dependencies](#development-dependencies).

#### Production Build

To compile both the JavaScript and CSS files, run:

```sh
yarn build
```

Alternatively, you could run `yarn build:js` and/or `yarn build:css`.  
`yarn build` simply runs these concurrently.

#### Development Build

A Development Build of the JavaScript and CSS files can be complied by running:

```sh
yarn build-dev
```

The development build differs from the production build in the folling ways:

* Source maps are available in the development build.
* Output files are not minimized in the development build.

## TypeScript and SCSS File Structure

TypeScript and SCSS files live in `./ts/src` and `./scss/src` respectively. Any
`.ts` and `.scss` files at the root level of these directories will be compile
into JavaScript and CSS respectively and made available in the directories
`./js` and `./css`.

Note: There are some `.ts` files not located in `./ts/src`.  
`./ts/rollup.config.ts` and `./scss/postcss.config.ts` are config files used to
configure how the source TypeScript and SCSS files are compiled.  
The `.ts` files located in `./scripts` are the script files that we run via
`yarn *`.

## Linting

Before running any linting test, first ensure that you have installed the
[Development Dependencies](#development-dependencies).

To run all the linting test on the commandline, run:

```sh
yarn lint
```

### PHP

PHP files are linted with
[CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) and the
[Coder](https://www.drupal.org/project/coder) module.

To lint PHP files on the commandline, run:

```sh
composer run-script lint:php
```

For convenience, `yarn lint:php` is also available as an alias.

### TypeScript

TypeScript files are linted with [TSLint](https://palantir.github.io/tslint).

To lint TypeScript files on the commandline, run:

```sh
yarn lint:ts
```

### SCSS

SCSS files are linted with [StyleLint](https://stylelint.io).

To lint SCSS files on the commandline, run:

```sh
yarn lint:scss
```

## Testing

To run the Drupal test files, this module needs to be installed on a site.

* Create a new drupal 7 site.
* Install and enabled this module.
* Ensure the testing module is enabled.
* Test can be run from /admin/config/development/testing.
