# Autocomplete Attribute (ac_attr)

## Contents of this File

- [Introduction](#introduction)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

This module allows site administrators to specify using simple rules based on a
form's structure, what the autocomplete attribute for each field should be.

> Autocomplete attributes are a way for you, the developer, to control how the
> browser should populate a given form field. For example, if you are expecting
> a street address you can hint to the browser that you are expecting it by
> using `autocomplete="address-line1"`. This prevents the browser from
> incorrectly guessing form fields on your website which can result in a poor
> user experience.
>
> -- <cite>[developers.google.com](https://developers.google.com/web/updates/2015/06/checkout-faster-with-autofill)</cite>

## Configuration

- Configure user permissions in Administration » People » Permissions:
  - The `administer autofill rules` permission gives access to the
    administration pages.
- Rules
  - The Rules form allows for setting patterns that correspond with a set token.
  - To make the best use of these rules, forms should be structured with meaning
    full keys and in a top down direction.
- Overrides
  - The Overrides form allows for setting an override for specific forms and
    fields.
- Settings
  - The settings form allows disabling this module for certain forms.

## Maintainers

Current maintainers:

- [Gold](https://www.drupal.org/u/Gold)
- [Rebecca Stevens](https://www.drupal.org/u/rebeccastevens)

This project has been sponsored by:

- Catalyst IT
  - [catalyst.net.nz](https://catalyst.net.nz/)
  - [drupal.org/catalyst-it](https://www.drupal.org/catalyst-it)
